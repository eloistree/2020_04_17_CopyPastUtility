using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ClipboardQuickCodedTimer : MonoBehaviour
{
    public float m_timeBetweenRefresh=0.1f;
    public UnityEvent m_tick;
    void Start()
    {
        InvokeRepeating("Refresh", 0, m_timeBetweenRefresh);
    }

    public void Refresh() {
        m_tick.Invoke();
    }
}
