using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinClipboardAppMono : AbstractClipboardMono
{
    public override string GetClipboardText()
    {
        return WinClipboardApp.Get();
    }


    public override void SetClipboardText(string text)
    {
        WinClipboardApp.Set(text);
    }
}