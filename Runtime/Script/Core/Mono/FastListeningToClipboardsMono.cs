﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;





public class FastListeningToClipboardsMono : MonoBehaviour
{
    public AbstractPreviousableClipboard m_unityClipboard;
    public ClipboardChange m_unityClipEvent;

    public AbstractPreviousableClipboard m_guiClipboard;
    public ClipboardChange m_guiClipEvent;

    public float m_autoCheckChange=0.1f;

    private void Awake()
    {
        m_unityClipboard.m_onChange += m_unityClipEvent.Invoke;
        m_guiClipboard.m_onChange += m_guiClipEvent.Invoke;
    }
    private void OnDestroy()
    {
        m_unityClipboard.m_onChange -= m_unityClipEvent.Invoke;
        m_guiClipboard.m_onChange -= m_guiClipEvent.Invoke;
    }
    void Start()
    {
        InvokeRepeating("Refresh", 0, m_autoCheckChange);
    }

    

    public void Refresh()
    {
        m_unityClipboard.SetTextAndNotify(TextEditorClipboard.Get());
        m_guiClipboard.SetTextAndNotify(GUIClipboard.Get());
    }
}
