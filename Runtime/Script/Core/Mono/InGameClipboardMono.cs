using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameClipboardMono : AbstractClipboardMono
{
    [TextArea(0, 20)]
    public string m_clipboardText;
    public override string GetClipboardText()
    {
        return WinClipboardApp.Get();
    }


    public override void SetClipboardText(string text)
    {
        WinClipboardApp.Set(text);
    }
}