using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextEditorClipboardMono : AbstractClipboardMono
{
    public override string GetClipboardText()
    {
        return TextEditorClipboard.Get();
    }
    public override void SetClipboardText(string text)
    {
        TextEditorClipboard.Set(text);
    }
}