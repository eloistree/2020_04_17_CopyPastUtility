using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIClipboardMono : AbstractClipboardMono
{
   

    public override string GetClipboardText()
    {
        return GUIClipboard.Get();
    }


    public override void SetClipboardText(string text)
    {
        GUIClipboard.Set(text);
    }

}
