using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameKeyCodeCopyPastListener : MonoBehaviour
{
    public AbstractCopyPastListener m_abstractListener;
    
    void Update()
    {

        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.X))
        {
            m_abstractListener.TriggerCut();
        }
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.C))
        {

            m_abstractListener.TriggerCopy();
        }
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.V))
        {
            m_abstractListener.TriggerPast();

        }

    }

    private void Reset()
    {
        m_abstractListener = GameObject.FindObjectOfType<AbstractCopyPastListener>();
    }
}
