using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AbstractCopyPastListener : MonoBehaviour ,ICopyPastListener
{
    public CopyPastEventFunction m_cutFound;
    public CopyPastEventFunction m_copyFound;
    public CopyPastEventFunction m_pastFound;

    public CopyPastEvent m_cutFoundEvent;
    public CopyPastEvent m_copyFoundEvent;
    public CopyPastEvent m_pastFoundEvent;


    public void TriggerCut()
    {
        if (m_cutFound != null)
            m_cutFound();
        m_cutFoundEvent.Invoke();
    }
    public void TriggerCopy()
    {
        if (m_copyFound != null)
            m_copyFound();
        m_copyFoundEvent.Invoke();
    }
    public void TriggerPast()
    {
        if (m_pastFound != null)
            m_pastFound();
        m_pastFoundEvent.Invoke();
    }

    public delegate void CopyPastEventFunction();
    [System.Serializable]
    public class CopyPastEvent : UnityEvent{};

}

public interface ICopyPastListener {

    void TriggerCut();
    void TriggerCopy();
    void TriggerPast();
}

public interface IClipboardChangeListener {

    void ClipBoardChange(string previous, string current);
}