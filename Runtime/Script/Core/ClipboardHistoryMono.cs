using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ClipboardHistoryMono : MonoBehaviour, IHistoryClipboard
{
    public AbstractClipboardMono m_clipboardTarget;

    public AbstractPreviousableClipboard m_currenPreviousKeeper;
    [Range(2, ushort.MaxValue)]
    public ushort m_memorySize = 2;
    public List<string> m_recentToLatest = new List<string>();


    [Header("Events")]
    public ClipboardChange m_onClipboardChange;
    public CopyPastEvent m_previousValue;
    public CopyPastEvent m_currentValue;




    public void ClearWithoutNotification() {

        m_currenPreviousKeeper.Clear();
        m_recentToLatest.Clear();
    }

    private void Awake()
    {
        m_currenPreviousKeeper.m_onChange += m_onClipboardChange.Invoke;
    }
    private void OnDestroy()
    {
        m_currenPreviousKeeper.m_onChange -= m_onClipboardChange.Invoke;
    }

    public string GetClipboardText()
    {
        return m_clipboardTarget.GetClipboardText();
    }

    private bool m_firstTime=true;
    public void CheckForChangeAndApply()
    {
        

        bool changed; string previous;
        string current = GetClipboardText();

        if (m_firstTime) {
            m_currenPreviousKeeper.HardSetWithNoNotification(current);
            m_firstTime = false;
            return;
        }

        m_currenPreviousKeeper.SetText(current, out changed, out previous);

        if (changed)
        {
            m_recentToLatest.Insert(0, current);
            if (m_recentToLatest.Count > m_memorySize)
            {
                m_recentToLatest.RemoveAt(m_recentToLatest.Count - 1);
            }

            m_currenPreviousKeeper.NotifyAsChanged();
            m_onClipboardChange.Invoke(m_currenPreviousKeeper.GetPreviousText(),m_currenPreviousKeeper.GetClipboardText());
            m_previousValue.Invoke( m_currenPreviousKeeper.GetPreviousText());
            m_currentValue.Invoke(m_currenPreviousKeeper.GetClipboardText());
        }
    }



    public IEnumerable<string> GetHistoryRecentToLast()
    {
        return m_recentToLatest;
    }

    public ushort GetMaxMemorySize()
    {
        return (ushort)m_recentToLatest.Count;
    }

    public string GetPreviousText()
    {
        return m_currenPreviousKeeper.GetPreviousText();
    }

    public void SetClipboardText(string text)
    {
        m_clipboardTarget.SetClipboardText(text);
    }

}


