using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class AbstractPreviousableClipboard : IPreviousableClipboard
{

    public string m_currentValue = "";
    public string m_previousValue = "";
    public ClipboardChangeFunction m_onChange;


    public void HardSetWithNoNotification(string current, string previous)
    {

        m_currentValue = current;
        m_previousValue = previous;
    }
    public void HardSetWithNoNotification(string current)
    {

        m_currentValue = current;
        m_previousValue = current;
    }

    public void SetTextAndNotify(string text)
    {
        bool changed; string previous;
        SetText(text, out changed, out previous);
        if (changed)
        {
            if (m_onChange != null)
                m_onChange(previous, text);
        }
    }

    public void SetText(string text, out bool changed, out string previous)
    {
        changed = IsTextDifferentOfCurrent(text);
        if (changed)
        {
            m_previousValue = m_currentValue;
            m_currentValue = text;
            previous = m_previousValue;
        }
        else
        {
            previous = m_currentValue;
        }
    }

    private bool IsTextDifferentOfCurrent(string text)
    {
        return text.Length != m_currentValue.Length || text.IndexOf(m_currentValue) != 0;
    }

    public string GetPreviousText()
    {
        return m_previousValue;
    }

    public void SetClipboardText(string text)
    {
        SetTextAndNotify(text);
    }

    public string GetClipboardText()
    {
        return m_currentValue;
    }

    public void NotifyAsChanged()
    {
        if (m_onChange != null)
            m_onChange(m_previousValue, m_currentValue);
    }

    public void Clear()
    {
        m_previousValue = m_currentValue = "";
    }
}
[System.Serializable]
/// Previous > current
public class ClipboardChange : UnityEvent<string, string> { }
public delegate void ClipboardChangeFunction(string previous, string current);