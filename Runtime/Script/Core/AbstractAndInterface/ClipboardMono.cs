﻿using UnityEngine;

public abstract class ClipboardMono : MonoBehaviour, ISimpleClipboard
{
    public abstract string GetClipboardText();
    public abstract void SetClipboardText(string text);

    
}
