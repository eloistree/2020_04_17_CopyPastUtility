using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISimpleClipboard
{
    void SetClipboardText(string text);
    string GetClipboardText();
}
public interface IPreviousableClipboard : ISimpleClipboard
{
    string GetPreviousText();
}
public interface IHistoryClipboard : IPreviousableClipboard
{
    IEnumerable<string> GetHistoryRecentToLast();
    ushort GetMaxMemorySize();
}

