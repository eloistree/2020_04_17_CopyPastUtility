using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractClipboardMono : MonoBehaviour, ISimpleClipboard
{


    public abstract string GetClipboardText();


    public abstract void SetClipboardText(string text);
}

