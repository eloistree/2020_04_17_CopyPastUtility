using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class CopyPastEvent : UnityEvent<string> { }

[System.Serializable]
public class AlteredCopyPastEvent : UnityEvent<string, string> { }
public delegate void AlteredCopyPastFunctionCall(string lastCopyText, string applyPastText);