﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class TextEditorClipboard
{

        //https://flystone.tistory.com/138
         public static string Get()
        {
            TextEditor _textEditor = new TextEditor();
             _textEditor.Paste();
            return _textEditor.text;
        }
        public static void Set(string value)
        {
            TextEditor _textEditor = new TextEditor
            { text = value };
            _textEditor.OnFocus();
            _textEditor.Copy();
        }
}

public class  WinClipboardApp {
    //Doc: https://www.c3scripts.com/tutorials/msdos/clip.html
    //Source: https://stackoverflow.com/a/58875512/13305320
    public static void Set(string value)
    {
        if (value == null)
            throw new ArgumentNullException("Attempt to set clipboard with null");

        Process clipboardExecutable = new Process();
        clipboardExecutable.StartInfo = new ProcessStartInfo
        {
            RedirectStandardInput = true,
            FileName = @"clip",
            UseShellExecute = false
        };
        clipboardExecutable.Start();
        clipboardExecutable.StandardInput.Write(value);
        clipboardExecutable.StandardInput.Close();

        return;
    }
    public static string Get()
    {
        //Not sure we can access the clipboard of window without Win Form.
        // WIll be add if I found a way to have direct access to it.
        //PS: not to future me or you. The reason this one is important is because Unity don't allow to recover link, image, media, files from the clipboard.
        //When at the origne that was why I wanted to create this code it.
        throw new NotImplementedException();
    }

}

public class GUIClipboard
{
    //https://answers.unity.com/questions/266244/how-can-i-add-copypaste-clipboard-support-to-my-ga.html

    public static string Get()
    {
            return GUIUtility.systemCopyBuffer;
    }
    public static void Set(string value)
    {
            GUIUtility.systemCopyBuffer = value;
    }
}