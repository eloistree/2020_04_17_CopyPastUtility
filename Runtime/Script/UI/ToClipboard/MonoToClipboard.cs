using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoToClipboard : MonoBehaviour
{
    public ClipboardType m_clipType;
    public void Push(string text)
    {
        ClipboardUtility.Set(text, m_clipType);

    }
}
