﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ExperimentFileToClipboard : MonoBehaviour
{
    public TextAsset m_text;
    public string m_packageManifest;

    public void Reset()
    {
           m_packageManifest = Application.dataPath+ "/../Packages/manifest.json";
    }
    public void PushTextFileToClipboard()
    {

        ClipboardUtility.PushFileFromTextAsset(m_text);
    }
    public void PushFilePathContentToClipboard()
    {

        ClipboardUtility.PushFileFromAbsolutePath(m_packageManifest);

    }
}

public enum ClipboardType { GUI, TextEditor, Window }
public class ClipboardUtility
{

    public static void PushFileFromAbsolutePath(string fileAbsolutPath, ClipboardType type = ClipboardType.GUI)
    {
        if (string.IsNullOrEmpty(fileAbsolutPath)) {
            return;
            //throw new System.ArgumentException("No path set to copy in clipboard");
        }
        if (!File.Exists(fileAbsolutPath)) {
            return;
            //throw new System.ArgumentException("No file found at: "+fileAbsolutPath);
        }
        
            Set(File.ReadAllText(fileAbsolutPath), type);
        
    }

    public static void PushFileFromDataProjectRelativePath(string relativePathFile, ClipboardType type = ClipboardType.GUI)
    {
        PushFileFromAbsolutePath(Application.dataPath + "/" + relativePathFile, type);

    }

    public static void PushFileFromTextAsset(TextAsset file, ClipboardType type = ClipboardType.GUI)
    {
        if (file != null)
        {
            Set(file.text, type);
        }
    }

    public static void Set(string value, ClipboardType type = ClipboardType.GUI)
    {
        switch (type)
        {
            case ClipboardType.GUI:
                GUIClipboard.Set(value);
                break;
            case ClipboardType.TextEditor:
                TextEditorClipboard.Set(value);
                break;
            case ClipboardType.Window:
                WinClipboardApp.Set(value);
                break;
            default:
                GUIClipboard.Set(value);
                break;
        }
    }
    public static string Get(ClipboardType type = ClipboardType.GUI)
    {
        switch (type)
        {
            case ClipboardType.GUI:
                return GUIClipboard.Get();
                
            case ClipboardType.TextEditor:
                return TextEditorClipboard.Get();
              
            case ClipboardType.Window:
                return WinClipboardApp.Get();
               
            default:
                return GUIClipboard.Get();
               
        }
    }

}