using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebpageToClipboard : MonoBehaviour
{
    public string m_urlTarget;
    public ClipboardType m_clipbaordType;

    public void PushWebPage()
    {

        PushWebPage(m_urlTarget);

    }
    public void PushWebPage(string url)
    {
        StartCoroutine(PushWebPageTask(url));

    }

    private IEnumerator PushWebPageTask(string url)
    {
        if (string.IsNullOrEmpty(url))
            yield return null;

        WWW page = new WWW(url);
        yield return page;

        ClipboardUtility.Set(page.text, m_clipbaordType);

    }
}
