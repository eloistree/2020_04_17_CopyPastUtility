using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ClipboardHistoryDebug : MonoBehaviour
{
   public  ClipboardHistoryMono m_linked;

    public InputField m_current;
    public InputField m_previous;

    public List<InputField> m_history = new List<InputField>();


    public void RefreshUI()
    {
        if (m_linked == null)
            return;

        if (m_current != null)
            m_current.text = m_linked.GetClipboardText();
        if(m_previous!=null)
            m_previous.text = m_linked.GetPreviousText();
        int i=0;
        foreach (string item in m_linked.GetHistoryRecentToLast())
        {
            if (i < m_history.Count && m_history[i] != null)
                m_history[i].text = item;
            i++;
        }

    }
}
