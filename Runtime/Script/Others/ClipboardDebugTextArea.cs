using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClipboardDebugTextArea : MonoBehaviour
{
    [TextArea(5,10)]
    public string m_current;

    [TextArea(5, 10)]
    public string m_previous;

    public void SetCurrentWith( string text)
    {
        if (text.Length > 1000)
            text.Substring(0, 1000);
        m_current = text;
    }
    public void SetPreviousWith( string text)
    {
        if (text.Length > 1000)
            text.Substring(0, 1000);
        m_previous = text;
    }


}
