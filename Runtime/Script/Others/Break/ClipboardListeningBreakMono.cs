using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClipboardListeningBreakMono : MonoBehaviour
{
    public static void PauseForMilliseconds(long milliseconds)
    {
        ClipboardListeningBreak.PauseForMilliseconds(milliseconds);

    }
    public static void PauseForSeconds(float seconds)
    {
        ClipboardListeningBreak.PauseForSeconds(seconds);
    }
    public void SetToggleBreak(bool breakOn) {
        ClipboardListeningBreak.SetBreak(breakOn);
    }
}


public class ClipboardListeningBreak {

    static ClipboardListeningBreak(){
        m_breakUntil = DateTime.Now;
    }
    private static bool m_totalBreak;
    private static DateTime m_breakUntil;
    public static void PauseForMilliseconds(long milliseconds) {
    

        DateTime waitUntil = DateTime.Now.AddMilliseconds(milliseconds);
        if (waitUntil > m_breakUntil)
            m_breakUntil = waitUntil;
    
    }
    public static void SetBreak(bool breakOn) {
        m_totalBreak = breakOn;
    }
    public static bool HasToggleBreak() {
        return m_totalBreak;
    }

    public static void PauseForSeconds(float seconds) {
        PauseForMilliseconds((long)(seconds * 1000f));
    }
    public static bool IsPaused()
    {
        return m_totalBreak || DateTime.Now < m_breakUntil;
    }
    public static DateTime GetBreakTime()
    {
        return m_breakUntil.AddSeconds(0);
    }
    public static double GetTimeLeftInSeconds()
    {
        return (m_breakUntil - DateTime.Now).TotalSeconds;
    }
}