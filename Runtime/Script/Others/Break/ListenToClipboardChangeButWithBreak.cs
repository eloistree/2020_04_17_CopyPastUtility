using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ClipboardChangeBean {


    [SerializeField] string m_previousValue;
    [SerializeField] string m_currentValue;

    public ClipboardChangeBean(string previousValue, string currentValue)
    {
        m_previousValue = previousValue;
        m_currentValue = currentValue;
    }
    public void ResetWith(string previousValue, string currentValue)
    {
        m_previousValue = previousValue;
        m_currentValue = currentValue;
    }
}
public class ListenToClipboardChangeButWithBreak : MonoBehaviour
{
    public ClipboardChangeBean m_wasBlockByBreak= new ClipboardChangeBean("","");


    public ClipboardChange m_notBlockedChange;
    public CopyPastEvent m_previousValue;
    public CopyPastEvent m_currentValue;
    public void ClipboardFound(string previous, string current) {
        if (ClipboardListeningBreak.IsPaused()) { 
            m_wasBlockByBreak.ResetWith(previous, current);
        }
        else {

            m_notBlockedChange.Invoke(previous, current);
            m_previousValue.Invoke(previous);
            m_currentValue.Invoke(current);
        }


    }

   
}
